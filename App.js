import React, { useState, useEffect } from "react"
import {
	View,
	FlatList,
	SafeAreaView,
	StyleSheet,
	ActivityIndicator,
} from "react-native"
import { Header } from "./components/Header"
import axios from "axios"
import { ListItem } from "./components/ListItem"

const memeURL = " http://alpha-meme-maker.herokuapp.com/1 "

const App = () => {
	const [isLoading, setLoading] = useState(true)
	const [data, setData] = useState([])

	useEffect(() => {
		axios
			.get(memeURL)
			.then(function (response) {
				setData(response.data.data.sort((a, b) => (a.name > b.name ? 1 : -1)))
			})
			.catch(function (error) {
				//TODO: add proper error handling
				console.log(error)
			})
			.finally(function () {
				setLoading(false)
			})
	}, [])

	return (
		<SafeAreaView style={styles.container}>
			<Header />
			{isLoading ? (
				<ActivityIndicator style={styles.activityInidicator} />
			) : (
				<View style={styles.content}>
					<View style={styles.list}>
						<FlatList
							keyExtractor={(item) => item.ID.toString()}
							data={data}
							renderItem={({ item }) => <ListItem item={item} />}
						/>
					</View>
				</View>
			)}
		</SafeAreaView>
	)
}

export default App

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#bdbdbd",
	},
	content: {
		flex: 1,
		backgroundColor: "gray",
	},
	list: {
		flex: 1,
		backgroundColor: "white",
		padding: 15,
	},
	activityInidicator: {
		flex: 1,
		alignContent: "center",
	},
})

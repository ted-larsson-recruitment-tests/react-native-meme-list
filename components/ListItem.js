import React, { useState } from "react"
import { StyleSheet, Text, View, Image, Pressable } from "react-native"

export const ListItem = ({ item }) => {
	const [selected, setSelected] = useState(false)

	return (
		<Pressable
			onPress={() => {
				setSelected(!selected)
			}}
			style={selected ? styles.selectedItemsContainer : styles.itemContainer}
		>
			<View>
				<Image source={item.image} style={styles.itemImage} />
			</View>
			<View style={styles.textContainer}>
				<Text style={styles.itemName}>{item.name}</Text>
				<Text style={styles.itemTags}>{item.tags}</Text>
			</View>
		</Pressable>
	)
}

const styles = StyleSheet.create({
	itemContainer: {
		flexDirection: "row",
		padding: 5,
		borderBottomWidth: 1,
		borderBottomColor: "#d9d9d9",
		marginBottom: 15,
	},
	selectedItemsContainer: {
		backgroundColor: "#ffd1b3",
		flexDirection: "row",
		padding: 5,
		borderBottomWidth: 1,
		borderBottomColor: " gray",
		marginBottom: 15,
	},
	textContainer: {
		flexShrink: 1,
	},
	itemImage: {
		width: 60,
		height: 60,
		marginRight: 15,
		borderWidth: 1,
		borderColor: "black",
	},
	itemName: {
		fontSize: 15,
		fontWeight: "bold",
		marginBottom: 5,
	},
	itemTags: {
		flex: 1,
		flexShrink: 1,
	},
})

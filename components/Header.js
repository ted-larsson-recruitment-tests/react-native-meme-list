import React from "react"
import { StyleSheet, Text, View } from "react-native"

export const Header = () => {
	return (
		<View style={styles.header}>
			<Text style={styles.title}> Meme List </Text>
		</View>
	)
}

const styles = StyleSheet.create({
	header: {
		height: 80,
		paddingTop: 40,
		backgroundColor: "#ff8533",
	},
	title: {
		fontSize: 20,
		fontWeight: "bold",
		color: "white",
		textAlign: "center",
	},
})
